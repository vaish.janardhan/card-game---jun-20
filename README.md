# Card Game - Jun 20

## Diamonds - A Game of Cards
Card Values : 2 - 10 as is, J - 11, Q - 12, K - 13, A - 14

3 players are distributed one suite each, one has all hearts, the other spades ...<br>
Diamonds are shuffled and the card on top is revealed each time. The players bid one card each from their suite and the player with the highest bid scores the value equivalent to the diamond card. In the case of players bidding same cards, the score gets divided.
A banker keeps a note of the scores and declares the winner in the end.

For a 2 player game, one suite can be discarded.