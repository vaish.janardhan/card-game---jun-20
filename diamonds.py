import random

def legalCount(card_value):
    value_to_count = {'A' : 14, 'K' : 13, 'Q' : 12, 'J' : 11 , '10' : 10,  '9' : 9,  '8' : 8,  '7' : 7,  '6' : 6,  '5' : 5,  '4' : 4, '3' : 3,  '2' : 2}
    return value_to_count[card_value]
    
def isLegalCard(card, card_list):
    return card in card_list

def scoreUpdate( comp_score, user_score, comp_card, user_card, diamond):
    if comp_card > user_card:
        comp_score += diamond
    elif user_card > comp_card:
        user_score += diamond
    else:
        comp_score += diamond/2
        user_score += diamond/2
    return comp_score, user_score

def getComputerCard(possible_cards, val_on_top):
    return random.choice(possible_cards)
    
def updatedCards (possible_cards, curr_card):
    possible_cards.remove(curr_card)
    return possible_cards

def getUserCard(user_cards):
    is_legal_card = False
    while not is_legal_card:
        user = input("Enter a card : ")
        user_card = legalCount(user)
        is_legal_card = isLegalCard(user_card, user_cards)
    return user_card

def getWinner(comp_score, user_score):
    if comp_score > user_score:
        return "Computer Wins"
    elif user_score > comp_score:
        return "User Wins"
    else:
        return "Tie"
    
def initGame():
    computer_cards = [i for i in range(2,15)]
    diamonds = [i for i in range(2,15)]
    user_cards = [i for i in range(2,15)]
    random.shuffle(diamonds)
    comp_score = 0
    user_score = 0
    return computer_cards, user_cards, comp_score, user_score, diamonds

def playLegalGame():
    computer_cards, user_cards, comp_score, user_score, diamonds = initGame()
    while len(diamonds) > 0:
        diamond = diamonds.pop()
        print(diamond," on top")
        computer = getComputerCard(computer_cards, diamond)
        user_card = getUserCard(user_cards)
        user_cards = updatedCards(user_cards, user_card)
        computer_cards = updatedCards(computer_cards, computer)
        print(getWinner(computer, user_card))
        comp_score, user_score = scoreUpdate(comp_score, user_score, computer, user_card, diamond)
    return comp_score, user_score

comp_score, user_card = playLegalGame()
print("Final Result ", getWinner(comp_score, user_card))